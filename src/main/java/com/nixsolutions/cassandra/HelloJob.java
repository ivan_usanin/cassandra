package com.nixsolutions.cassandra;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.List;

/**
 * Created by usanin on 5/30/2016.
 */
public class HelloJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<String> list = new CassandraJob().getString();
        CamelServiceFile service = null;
        try {
            service = new CamelServiceFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (String text : list) {
            service.send(text);
            System.out.println(text);
        }
    }
}
