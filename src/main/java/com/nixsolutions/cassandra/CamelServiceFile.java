package com.nixsolutions.cassandra;

import static org.apache.activemq.camel.component.ActiveMQComponent.activeMQComponent;

import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

/**
 * Created by usanin on 5/30/2016.
 */
public class CamelServiceFile {

    private static final String IN_ENDPOINT = "activemq:queue:file-queue";
    private static final String OUT_ENDPOINT = "file://test";

    private CamelContext context;
    private ProducerTemplate producer;
    private ConsumerTemplate consumer;

    public CamelServiceFile() throws Exception {
        context = new DefaultCamelContext();

        context.addRoutes(new RouteBuilder() {

            @Override
            public void configure() throws Exception {
                from(IN_ENDPOINT).to(OUT_ENDPOINT);
            }
        });

        context.addComponent("activemq", activeMQComponent("tcp://localhost:61616"));

        producer = context.createProducerTemplate();
        consumer = context.createConsumerTemplate();

        context.start();
    }

    public void send(final String message) {
        producer.sendBody(IN_ENDPOINT, message);
    }

    public String receive() {
        return (String) consumer.receiveBody(OUT_ENDPOINT);
    }
}
