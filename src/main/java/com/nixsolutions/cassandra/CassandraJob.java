package com.nixsolutions.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by usanin on 5/30/2016.
 */
public class CassandraJob {

    public List<String> getString() {
        List<String> toReturn = new ArrayList<>();
        Cluster cluster;
        Session session;
        cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
        try {
            session = cluster.connect("mykeyspace");
            //session.execute("INSERT INTO users (user_id, fname, lname) VALUES (1715, 'Bob', 'Austin')");
            ResultSet results = session.execute("SELECT * FROM users");
            for (Row row : results) {
                toReturn.add(row.getString("fname") + " " + row.getString("lname"));
                //System.out.println(row.getString("fname") + " " + row.getString("lname"));
            }
        } finally {
            cluster.close();
        }
        return toReturn;
    }
}
